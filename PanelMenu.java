
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 * Cette classe permet d'afficher le menu avec comme option : nouvelle partie, voir les règles, quitter le jeu.
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 *
 */
public class PanelMenu extends JPanel implements ActionListener
{
	private IHM     frame;

	private PanelCreerPartie panelCreerPartie;
	private PanelRegle panelRegle;
	
	private JButton nouvellePartie;
	private JButton regle;
	private JButton quitter;
	
	public PanelMenu( IHM frame ) {
		
		this.frame = frame;
		
		this.setLayout( new GridBagLayout() );
       
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets( 10, 10, 10, 10 );


        this.nouvellePartie  = new JButton( "nouvelle partie" );
        this.regle           = new JButton( "règle du jeu" );
        this.quitter         = new JButton( "quitter" );

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 0;
        this.nouvellePartie.addActionListener( this );
        this.add( this.nouvellePartie, gbc );

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 1;
        this.regle.addActionListener( this );
        this.add( this.regle, gbc );

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 1;
        gbc.gridy = 2;
        this.quitter.addActionListener( this );
        this.add( this.quitter, gbc );
    
	}
	
	public void actionPerformed(ActionEvent e) {
		
		if ( e.getSource() == this.nouvellePartie ) {
			this.frame.redimensionnerFrame( 1000 , 600 );
			this.frame.setResizable( true );
			this.panelCreerPartie = new PanelCreerPartie( this.frame );
			this.frame.setContentPane( panelCreerPartie );
		}
		
		if ( e.getSource() == this.regle ) {
			this.frame.redimensionnerFrame( 1000, 600 );
			this.frame.setResizable( true );
			this.panelRegle = new PanelRegle( this.frame );
			 JScrollPane jsp = new JScrollPane( panelRegle , 
	                    ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, 
	                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED );
			jsp.setPreferredSize( new Dimension( 1000 , 600 ) );
			this.frame.setContentPane( jsp );
		}

        if ( e.getSource() == this.quitter ) {
            this.frame.setVisible(false);
            this.frame.dispose();
            System.exit(0);
        }
    }

	public void maj()
	{
		this.repaint();
		this.revalidate();
		if( this.panelCreerPartie != null ) this.panelCreerPartie.maj();
	}
}
