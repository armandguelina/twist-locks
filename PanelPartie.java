

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 * Cette classe représente le panel de la partie
 * 
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 * 
 */
public class PanelPartie extends JPanel implements ActionListener
{
	private IHM      frm;
	private Joueur[] tabJoueur;
	
	private PanelJoueur[] tabPanelJoueur;
	private PanelDessin   panelDessin;
	
	private JLabel     lblJoueur;
	private JLabel     lblTwistLock;
	private JTextField txtSaisie;
	private JButton    btnSaisie;

	public PanelPartie( IHM frm )
	{
		/*----- Initialisation des variables */
		this.frm = frm;
		this.tabJoueur = this.frm.getTabJoueur();
		
		this.tabPanelJoueur = new PanelJoueur[this.tabJoueur.length];
		for( int cpt = 0 ; cpt < this.tabPanelJoueur.length ; cpt++)
			this.tabPanelJoueur[cpt] = new PanelJoueur( this.frm , this.tabJoueur[cpt]);
		
		this.panelDessin = new PanelDessin( this.frm );
		this.lblJoueur = new JLabel( "C'est au joueur : " + this.frm.getJoueurCourant().getNom() );
		this.lblTwistLock = new JLabel( "Il vous reste : "+ this.frm.getJoueurCourant().getNbTwistRestant() );
		this.txtSaisie = new JTextField(30);
		this.txtSaisie.addActionListener( this );
		this.btnSaisie = new JButton("Valider");
		JPanel panelGauche = new JPanel();
		JPanel panelDroite = new JPanel();
		JPanel panelBas = new JPanel();
		JPanel panelTmp = new JPanel();
		JLabel lblTmp = new JLabel( "<html><body>Veuillez saisir les coordonées ou poser votre Twist-Lock <br/>1-"+this.frm.getTabConteneur().length+":A-"+(char)(this.frm.getTabConteneur()[0].length-1+'A')+":1-4"+"<br/>(numLigne:charCol:Angle)</body></html>" );
		
		/*----- Personnalisation -----*/
		this.setLayout( new BorderLayout() );
		
		panelBas.setLayout( new GridLayout( 2,2 ) );
		panelBas.setSize( this.getWidth() , 300 );
		
		//                                       1 ligne  si 2
		//                                       2 lignes si 3 ou 4 (this.tabJoueur.length%2)+(this.tabJoueur.length/2)
		if( this.tabJoueur.length > 2 )
			panelGauche.setLayout( new GridLayout( 2 , 1 , 50 , 50 ));
		else
			panelGauche.setLayout( new GridLayout( 1 , 1 , 50 , 50 ));
		panelGauche.setPreferredSize( new Dimension(250 , 400) );
		
		//                                      1 ligne  si 2 ou 3
		//                                      2 lignes si 4this.tabJoueur.length/2
		if( this.tabJoueur.length > 3 )
			panelDroite.setLayout( new GridLayout( 2 , 1 , 50 , 50 ));
		else
			panelDroite.setLayout( new GridLayout( 1 , 1 , 50 , 50 ));
		panelDroite.setPreferredSize( new Dimension(250 , 400) );
		

		this.lblJoueur.setFont(  new Font( "Arial", Font.BOLD, 20 ) ); 
		this.lblTwistLock.setFont(  new Font( "Arial", Font.BOLD, 20 ) );
		lblTmp.setFont(new Font( "Arial", Font.BOLD, 20 ));
		
		/*----- ActionListener -----*/
		
		this.btnSaisie.addActionListener( this );
		
		/*----- Ajout -----*/
		
		panelTmp.add( this.txtSaisie );
		panelTmp.add( this.btnSaisie );
		
		panelBas.add( this.lblJoueur );
		panelBas.add( lblTmp );
		panelBas.add( this.lblTwistLock );
		panelBas.add( panelTmp );

		// Ajoute 1 panel a gauche si 2 joueur
		panelGauche.add( this.tabPanelJoueur[0] );
		// Ajoute 1 panel a droite si 2 ou 3 joueur
		panelDroite.add( this.tabPanelJoueur[1] );
		if( this.tabJoueur.length > 2 )
		{
			// Ajoute 2 panel a gauche si 3 ou 4 joueur
			panelGauche.add( this.tabPanelJoueur[2] );
			if( this.tabJoueur.length > 3 )
				// Ajoute 2 panel a droite si 4 joueur
				panelDroite.add( this.tabPanelJoueur[3] );
		}
		
		this.add( panelGauche , BorderLayout.WEST );
		this.add( this.panelDessin , BorderLayout.CENTER );
		this.add( panelDroite , BorderLayout.EAST );
		this.add( panelBas , BorderLayout.SOUTH );
		this.maj();
	}

	public Joueur getJoueur( int numJoueur )
	{
		return this.frm.getTabJoueur()[numJoueur];
	}

	public Joueur[] getTabJoueur()
	{
		return this.frm.getTabJoueur();
	}

	public Conteneur[][] getTabConteneur()
	{
		return this.frm.getTabConteneur();
	}

	public void maj()
	{
		this.repaint();
		this.revalidate();
		this.lblJoueur.setText( "C'est au joueur : " + this.frm.getJoueurCourant().getNom() );
		this.lblTwistLock.setText( "Il vous reste : "+ this.frm.getJoueurCourant().getNbTwistRestant() );
		// for( int cpt = 0 ; cpt < this.tabPanelJoueur.length ; cpt++ )
		// this.tabPanelJoueur[cpt].maj();
		// this.panelDessin.maj();
	}

	@Override
	public void actionPerformed( ActionEvent e )
	{
		this.frm.placerTwistLock( this.txtSaisie.getText() );
		//this.txtSaisie.setText( null ); // A mettre si on veut que text soit vide à chaque fois , enlever en mode debug

	}
}
