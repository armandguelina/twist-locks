




/**
 * Cette classe est le controleur. Elle fait le lien entre le métier (le tablier) et la frame (ihm) selon le model MVC
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 *
 */
public class Controleur
{
	private IHM ihm;
	private Tablier tablier;
	
	public Controleur()
	{
		this.ihm = new IHM( this ); 
		this.tablier = new Tablier( this );
	}
	
	public void lancerPartie( String[] ensNomJoueur , int nbLig ,int nbCol , int nbTwist )
	{
		this.tablier.lancerPartie( ensNomJoueur , nbLig , nbCol , nbTwist );
	}
	
	public Joueur[] getTabJoueur()
	{
		return this.tablier.getTabJoueur();
	}
	
	public Conteneur[][] getTabConteneur()
	{
		return this.tablier.getTabConteneur();
	}
	
	public static void main( String[] args )
	{
		new Controleur();
	}

	public void placerTwistLock( String text )
	{
		this.tablier.placerTwistLock(text);
		
	}

	public int getNbLigne()
    {
        return this.tablier.getNbLigne();
    }

    public int getNbColonne()
    {
        return this.tablier.getNbColonne();
    }
	
	public Joueur getJoueurCourant()
	{
		return this.tablier.getJoueurCourant();
	}

	public void finDePartie( String strFin )
	{
		this.ihm.finDePartie( strFin );
	}

	public void maj()
	{
		this.ihm.maj();
	}
}
