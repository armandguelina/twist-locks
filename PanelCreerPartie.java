

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


/**
 * Cette class permet d'afficher un panel ou les joueurs rentrent des informations concernant la partie :
 * le nombre de joueurs / leurs noms / le nombre de lignes et de colonnes / le nombre de twistlock par personne
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 *
 */
public class PanelCreerPartie extends JPanel implements ActionListener 
{
	private IHM     frame;
	private JButton valider;
	private PanelPartie panelPartie;
	
	private String[] alCouleur    = { "ROUGE", "VERT ", "BLEU ", "JAUNE" };
	private Color[]  alColor      = { Color.RED, Color.GREEN, Color.BLUE, Color.yellow };
	private String[] alLabelParam = { "nombre de colone:", "nombre de ligne: ", "nombre de twistlock/joueur:" };
	
	private PanelJoueurInfo[] alPanelJoueur;
	
	// [0] = nbColonne ; [1] = nbLigne ; [2] = nb twistlock/joueur
	private JTextField[] alParametrage;
	
	public PanelCreerPartie( IHM frame )
	{
		this.frame = frame;
		
		this.setLayout( new GridBagLayout() );
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets( 20, 40, 20, 40 );
		
		this.alPanelJoueur = new PanelJoueurInfo[ alCouleur.length ];
		boolean facultatif = false;
		gbc.gridx = 0;
		for ( int i=0; i<alPanelJoueur.length; i++ )
		{
			gbc.gridy = i;
			if ( i == 2 ) facultatif = true;
			this.alPanelJoueur[i] = new PanelJoueurInfo( "Joueur " + alCouleur[i], facultatif, alColor[i] );
			
			this.add(  this.alPanelJoueur[i], gbc );
		}
		
		for ( int i=2; i<alPanelJoueur.length; i++ )
		{
			if ( i == 2 ) this.alPanelJoueur[i].setJoueurPrecedent( null );
			else          this.alPanelJoueur[i].setJoueurPrecedent( this.alPanelJoueur[i-1] );
			if ( i == this.alPanelJoueur.length-1 ) this.alPanelJoueur[i].setJoueurSuivant( null );
			else                                    this.alPanelJoueur[i].setJoueurSuivant( this.alPanelJoueur[i+1] );
		}
		
		
		this.alParametrage = new JTextField[3];
		
		for ( int i=0; i<this.alParametrage.length; i++ )
		{
			JLabel jL1 = new JLabel( alLabelParam[i] );
			jL1.setFont( new Font( "Arial", Font.BOLD, 13 ) );
			gbc.gridy = i;
			gbc.insets = new Insets( 20, 40, 20, 10 );
			gbc.gridx = 1;
			this.add( jL1, gbc );
			
			gbc.insets = new Insets( 20, 10, 20, 40 );
			gbc.gridx = 2;
			this.alParametrage[i] = new JTextField( 3 );
			this.alParametrage[i].addActionListener( this );
			this.add( this.alParametrage[i], gbc );
		}
		
		
		
		this.valider = new JButton("Valider");
		this.valider.addActionListener( this );
		gbc.gridwidth = 2;
		gbc.gridx = 1;
		gbc.gridy ++;
		this.add( this.valider , gbc );
		
	}
	
	public void actionPerformed( ActionEvent event )
	{
	    
	    if ( event.getSource() == this.valider )
	    	informationsPartie();
	    
	}
	
	public void informationsPartie()
	{
		
		int nbJoueurs = 0;
		for ( int i=0; i<this.alPanelJoueur.length; i++)
		{			
			if ( this.alPanelJoueur[i].estActif() )
			{
				if ( this.alPanelJoueur[i].getNom().length() == 0 )
				{
					System.out.println( "Il manque un nom." );
					return;
				}
				nbJoueurs++;
			}
		}
		
		String[] alNomJoueur = new String[ nbJoueurs ];
		for ( int i=0; i<nbJoueurs; i++ )
			alNomJoueur[i] = this.alPanelJoueur[i].getNom();
		
		for ( int i=0; i<alNomJoueur.length; i++ )
			for ( int j=0; j<alNomJoueur.length; j++ )
				if ( alNomJoueur[i].equals( alNomJoueur[j]) && i != j )
				{
					System.out.println( "Deux noms sont identiques." );
					return;
				}
						
		int nbCol, nbLig, nbTwistlock;
		try
        {
			// mettre maxi
            nbCol = Integer.parseInt( this.alParametrage[0].getText() );
            if ( nbCol > 13 ) Integer.parseInt( "Go au catch" );
            if ( nbCol <  5 ) Integer.parseInt( "Go au catch" );
        }
        catch( Exception e ){ nbCol = (int)( Math.random()*(13-5)+5 ); }

        try
        {
			// mettre maxi
            nbLig = Integer.parseInt( this.alParametrage[1].getText() );
            if ( nbLig > 13 ) Integer.parseInt( "Go au catch" );
            if ( nbLig <  5 ) Integer.parseInt( "Go au catch" );
        }
        catch( Exception e ){ nbLig = (int)( Math.random()*(13-5)+5 ); }

        try
        {
			// mettre maxi
            nbTwistlock = Integer.parseInt( this.alParametrage[2].getText() );
            if ( nbTwistlock > ((nbLig+1)*(nbCol+1))/nbJoueurs ) Integer.parseInt( "Go au catch" );
            if ( nbTwistlock < 0 ) nbTwistlock = 20;
        }
        catch( Exception e )
        { 
        	if ( 20 > ((nbLig+1)*(nbCol+1))/nbJoueurs ) nbTwistlock = 10;
        	else                                        nbTwistlock = 20; 
        }
		
		this.frame.lancerPartie( alNomJoueur, nbLig , nbCol , nbTwistlock );

		this.frame.modePleinEcran();
        
		this.panelPartie = new PanelPartie( this.frame );	
		this.frame.setContentPane( this.panelPartie );
        
        this.frame.setResizable( true );
        this.frame.revalidate();
	}

	public void maj()
	{
		this.repaint();
		this.revalidate();
		if( this.panelPartie != null ) this.panelPartie.maj();
	}
}



class PanelJoueurInfo extends JPanel implements ActionListener
{
	private boolean facultatif;
	
	private JTextField nom;
	
	private ButtonGroup  buttonG;
	private JRadioButton br1;
	private JRadioButton br2;
	
	private PanelJoueurInfo joueurPrecedent;
	private PanelJoueurInfo joueurSuivant;
		
	public PanelJoueurInfo( String couleur, boolean facultatif, Color coul )
	{
		this.joueurPrecedent = null;
		this.joueurSuivant   = null;
		
		this.facultatif = facultatif;
		
		this.setBackground( coul );
		this.setLayout( new GridBagLayout() );
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets( 10, 10, 10, 50 );
		
		
		JLabel j1 = new JLabel( couleur );
		j1.setFont( new Font( "Arial", Font.BOLD, 20 ) );
		gbc.gridwidth = 3;
		gbc.gridx = 0;
        gbc.gridy = 0;
		this.add( j1, gbc );
		
		JLabel j2 = new JLabel("nom : ");
		j2.setFont( new Font( "Arial", Font.BOLD, 20 ) );
		gbc.insets = new Insets( 10, 10, 10, 10 );
		gbc.gridwidth = 1;
		gbc.gridx = 5;
		this.add( j2 , gbc );
		
		this.nom = new JTextField( 20 );
		this.nom.addActionListener( this );
		
		gbc.gridx ++;
		this.add( this.nom, gbc );
		
		
		if ( this.facultatif )
		{
			this.buttonG = new ButtonGroup();
			this.br1     = new JRadioButton("actif");
			this.br2     = new JRadioButton("inactif");
			
			this.br1.setFont( new Font( "Arial", Font.BOLD, 15 ) );
			this.br2.setFont( new Font( "Arial", Font.BOLD, 15 ) );
			
			this.br1.setBackground( coul );
			this.br2.setBackground( coul );
			
			this.br2.setSelected( true );
			this.nom.setEditable( false );
			
			this.buttonG.add(br1);
			this.buttonG.add(br2);
			
			this.br1.addActionListener(this);
			this.br2.addActionListener(this);
		
			gbc.insets = new Insets( 5, 10, 10, 10 );
			gbc.gridwidth = 1;
			gbc.gridx = 0;
	        gbc.gridy = 1;
			this.add( this.br1, gbc );
			
			gbc.insets = new Insets( 5, 10, 10, 50 );
			gbc.gridx ++;
			gbc.gridy = 1;
			this.add( this.br2, gbc );
		}
		
	}
	
	public void actionPerformed( ActionEvent event )
	{
		if ( event.getSource() == this.br1 )
	    {
	    	if ( this.joueurPrecedent != null && this.joueurPrecedent.br2.isSelected() )
	    	{
	    		this.joueurPrecedent.br1.setSelected( true );
	    		this.br2.setSelected( true );
	    	}
	    	else	    		
	    		this.nom.setEditable( true );
	    }
	       
	    
	    if ( event.getSource() == this.br2 )
	    {
	    	if ( this.joueurSuivant != null && this.joueurSuivant.br1.isSelected() )
    		{
    			this.joueurSuivant.br2.setSelected( true );
    			this.br1.setSelected( true );
    		}
	    	else
	    	{
	    		this.nom.setText( null );
	    		this.nom.setEditable( false );
	    	}
	    		
	    }
	}
	
	public void setJoueurPrecedent( PanelJoueurInfo precedent )
	{
		this.joueurPrecedent = precedent;
	}
	
	public void setJoueurSuivant( PanelJoueurInfo suivant )
	{
		this.joueurSuivant = suivant;
	}
	
	public String getNom()
	{
		return this.nom.getText();
	}
	
	public boolean estActif()
	{
		if ( !this.facultatif )
			return true;
		else if ( this.br1.isSelected() )
			return true;
		return false;
	}
}
