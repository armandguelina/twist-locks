
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;




/**
 * Cette classe permet d'afficher le plateau du jeu avec tous les conteneurs et les twistlocks.
 * Il s'adapte à la dimention de la frame.
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 *
 */
public class PanelDessin extends JPanel
{
	private static Color[] couleurJoueur = { Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW };
	private IHM frame;
	public PanelDessin( IHM frame )
	{
		this.frame = frame;
		this.maj();
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g); // tableau blanc
		this.removeAll();
		affichage( g );
	}
	
	public void affichage( Graphics g )
	{
		
		BufferedImage imageMap;
		try 
        {
            imageMap = ImageIO.read(new File( "ticket.png" ));
            
            int xPanel = this.getWidth();
            int yPanel = this.getHeight();
            
            int nbLigne   = this.frame.getNbLigne();
    		int nbColonne = this.frame.getNbColonne();
            
            // taille image : x = 100 | y = 50
            int xConteneur = xPanel/nbColonne - 5;
            int yConteneur = (yPanel-100)/nbLigne;
            
            Conteneur[][] tabConteneur = this.frame.getTabConteneur(); 
           
            this.setFont( new Font( "Arial", Font.BOLD, 20 ) );
    		
            for ( int i=0; i<nbLigne; i++ )
    		{
    			for ( int j=0; j<nbColonne; j++ )
    			{
    				// dessin conteneur
    				g.drawImage(imageMap, 5 + ( xConteneur * j ) + ( 5 * j ) , 5 + ( yConteneur * i ) + ( 5 * i ), xConteneur, yConteneur, null );
    				
    				// dessin couleur des conteneurs
    				if ( tabConteneur[i][j].getAppartenance() != 0 )
    				{
    					g.setColor( couleurJoueur[ tabConteneur[i][j].getAppartenance() - 1 ] );
        				int xOval = 5 + ( xConteneur * j ) + ( 5 * j ) + xConteneur/2 - 20 ;
        				int yOval = 5 + ( yConteneur * i ) + ( 5 * i ) + yConteneur/2 - 20;
        				g.fillOval( xOval , yOval , 40 , 30 );
	 				}

    				// dessin du numero
    				g.setColor( Color.BLACK );
    				String numero = String.format( "%02d" , tabConteneur[i][j].getNumConteneur() );
    				
    				int xNumero = 5 + ( xConteneur * j ) + ( 5 * j ) + xConteneur/2 - 10;
    				int yNumero = 5 + ( yConteneur * i ) + ( 5 * i ) + yConteneur/2 + 5;
    				g.drawString( numero , xNumero , yNumero );
    				
    				
    				// dessin des twistlock
    				int widthOval  = (int) ( xConteneur * 0.3 / 2.8 ) * 2;
					int heightOval = (int) ( yConteneur * 0.3 / 1.5 ) * 2;
					
					if ( tabConteneur[i][j].getTwistLock( 0 ) != 0 )
					{
						//System.out.println( "I = " + i + " J = " + j );
						g.setColor( couleurJoueur[ tabConteneur[i][j].getTwistLock( 0 ) -1] );
						
						int xOval = 5 + ( xConteneur * j ) + ( 5 * j ) - widthOval/2 -2;
	    				int yOval = 5 + ( yConteneur * i ) + ( 5 * i ) - heightOval/2-2;
	    				g.fillOval( xOval , yOval , widthOval , heightOval );
					}
					
    				if ( tabConteneur[i][j].getTwistLock( 1 ) != 0 && j == nbColonne-1 )
    				{
    					g.setColor( couleurJoueur[ tabConteneur[i][j].getTwistLock( 1 ) -1] );
    					int xOval = 5 + ( xConteneur * (j+1) ) + ( 5 * j ) - widthOval/2;
	    				int yOval = 5 + ( yConteneur * i ) + ( 5 * i ) - heightOval/2;
	    				g.fillOval( xOval , yOval , widthOval , heightOval );
    				}
    				
    				if ( tabConteneur[i][j].getTwistLock( 3 ) != 0 && i == nbLigne-1 )
    				{
    					g.setColor( couleurJoueur[ tabConteneur[i][j].getTwistLock( 3 ) -1 ] );
    					int xOval = 5 + ( xConteneur * j ) + ( 5 * j ) - widthOval/2;
	    				int yOval = 5 + ( yConteneur * (i+1) ) + ( 5 * i ) - heightOval/2;
	    				g.fillOval( xOval , yOval , widthOval , heightOval );
    				}
    				
    				if ( tabConteneur[i][j].getTwistLock( 2 ) != 0 && i == nbLigne-1 && j == nbColonne-1 )
    				{
    					g.setColor( couleurJoueur[ tabConteneur[i][j].getTwistLock( 2 ) -1 ] );
    					int xOval = 5 + ( xConteneur * (j+1) ) + ( 5 * j ) - widthOval/2;
	    				int yOval = 5 + ( yConteneur * (i+1) ) + ( 5 * i ) - heightOval/2;
	    				g.fillOval( xOval , yOval , widthOval , heightOval );
    				}
    				
    			}
    			
    			
    		}
        
        } catch (IOException e) { e.printStackTrace(); }
		
	}
	
	public void maj()
	{
		repaint();
	}
}
