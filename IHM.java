
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;




/**
 * Cette class est la JFrame sur lequel repose tous les panels. Elle fait aussi le lien entre les panels et le controleur
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 *
 */
public class IHM extends JFrame
{
	private Controleur ctrl;
	private PanelMenu panelMenu;
	
	public IHM( Controleur ctrl )
	{
		this.ctrl = ctrl;
		
		this.setTitle("twistlock");

		this.redimensionnerFrame( 500 , 600 );
		
		this.panelMenu =  new PanelMenu( this );
		this.add( this.panelMenu );
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setResizable( false );
		this.setVisible(true);
	}
	
	public void redimensionnerFrame(int x, int y){

		//Recupère les dimensions de l'écran
		Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		int yEcran = (int) dimension.getHeight();
		int xEcran = (int) dimension.getWidth();

		this.setLocation( xEcran / 2 - ( x / 2 ), yEcran / 2 - ( y / 2 ) );
		this.setSize( x, y );
	}
	
	public void lancerPartie( String[] ensNomJoueur, int nbLig, int nbCol ,  int nbTwistlock )
	{
		this.ctrl.lancerPartie( ensNomJoueur, nbLig , nbCol , nbTwistlock );
	}

	public Joueur[] getTabJoueur()
	{
		return this.ctrl.getTabJoueur();
	}
	
	public Conteneur[][] getTabConteneur()
	{
		return this.ctrl.getTabConteneur();
	}

	public void placerTwistLock( String text )
	{
		this.ctrl.placerTwistLock( text );
		
	}

	public Joueur getJoueurCourant()
	{
		return this.ctrl.getJoueurCourant();
	}

	public void maj()
	{
		this.repaint();
		this.revalidate();
		this.panelMenu.maj();
	}

	public void finDePartie(String strFin)
	{
		try{ 
			JPanel panelTmp = new JPanel(  );
			JLabel lblTmp   = new JLabel( "<html><body>FINI !<br/> Le(s) meilleur(s) est(sont) : "+strFin+"</body></html>" );
			
			lblTmp.setFont(new Font( "Arial", Font.BOLD, 40 ));
			panelTmp.add( lblTmp  );
			
			this.setContentPane( panelTmp );
			this.repaint();
			this.revalidate();
		} catch( Exception e ) { e.printStackTrace(); }
	}
	
	public int getNbLigne()
    {
        return this.ctrl.getNbLigne();
    }

    public int getNbColonne()
    {
        return this.ctrl.getNbColonne();
    }
    
    public Joueur getJoueur( int numJoueur )
	{
		return this.ctrl.getTabJoueur()[numJoueur];
	}
    
    public void modePleinEcran()
    {
		GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		if ( device.isFullScreenSupported() )
			device.setFullScreenWindow( this );
		else 
			System.err.println("Le mode plein ecran n'est pas disponible");
    }
}
