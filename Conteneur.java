

/**
 * Cette classe représente un conteneur avec une valeur et un ensemble de coin
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 * 
 */
public class Conteneur
{
	private static int nbContenur = 0;
	private int idConteneur;
	private int numConteneur;
	
	// [0] = Haut Gauche
	// [1] = Haut Droite
	// [2] = Bas Droite
	// [3] = Bas Gauche
	//     = numJoueur ( 1 , 2 , 3 , 4 )
	//     2----0
	//     |    |
	//     1----1
	private int[] ensCoin;
	
	private Tablier tablier;
	
	public Conteneur( Tablier tablier )
	{
		this.tablier      = tablier;
		this.idConteneur  = Conteneur.nbContenur++;
		this.numConteneur = (int) ( Math.random() * ( 54 - 5 ) + 5 ) ;
		this.ensCoin      = new int[4];
	}
	
	public Conteneur( Conteneur ctn )
	{
		this.tablier      = ctn.tablier;
		this.idConteneur  = ctn.idConteneur;
		this.numConteneur = ctn.numConteneur;
		
		this.ensCoin = new int[4];
		for( int cpt = 0 ; cpt < this.ensCoin.length ; cpt++ )
			this.ensCoin[cpt] = ctn.ensCoin[cpt];

	}
	
	public int getAppartenance()
	{
		// [0] = J1 | [1] = J2 | ...
		int[] nbPionParJoueur = new int[ this.tablier.getNbJoueur() ];
		
		
		int nbCoinVide = 0;
		for ( int i=0; i<this.ensCoin.length; i++ )
			if ( this.ensCoin[i] != 0 )
				nbPionParJoueur[ this.ensCoin[i]-1 ] ++;
			else
				nbCoinVide ++;
		
		if ( nbCoinVide == 4 ) return 0;
		
		int numJoueur = 0;
		for ( int i=0; i<nbPionParJoueur.length; i++ )
			if ( nbPionParJoueur[i] > nbPionParJoueur[ numJoueur ] && i != numJoueur )
				numJoueur = i;
		
		for ( int i=0; i<nbPionParJoueur.length; i++ )
			if ( nbPionParJoueur[i] == nbPionParJoueur[ numJoueur ] && i != numJoueur )
				return 0;
		
		return numJoueur+1;
	}
	
	public boolean coinEstLibre( int coin )
	{
		return this.ensCoin[coin]==0;
	}
	
	public int getIdConteneur()
	{
		return this.idConteneur;
	}
	
	public int getNumConteneur()
	{
		return this.numConteneur;
	}
	
	public int getTwistLock( int pos )
	{
		return this.ensCoin[pos];
	}
	
	public boolean setTwistLock( int pos , int joueur )
	{
		if(this.ensCoin[pos] != 0 )
			return false;

		this.ensCoin[pos] = joueur;
		return true;
	}
}
