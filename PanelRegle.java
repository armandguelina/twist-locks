

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Cette classe permet d'afficher les règles du jeu.
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 *
 */
public class PanelRegle extends JPanel implements ActionListener
{
	private IHM     frame;

	private JButton retour;
	
	public PanelRegle( IHM frame ) {
		
		this.frame = frame;
		this.setLayout( new GridBagLayout() );
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		
        JLabel texte = new JLabel( "<html>"
				+ "<h2>1- Principe du JEU </h2>"
				+ "Un twist-Lock ou Verrou Tournant est une pièce de métal servant à solidariser des conteneurs pour leur transport.<br/>" 
				+ "Le jeu du Twist-Lock s’inspire de la pose de ces pièces métalliques par les apprentis dockers.<br/>"
				+ "Le jeu se déroule sur un pont à niveau unique de conteneur représentant un tablier généralement de 10 lignes par 7<br/>"
				+ "colonnes, mais dans la version du jeu qui vous concerne, les dimensions seront aléatoires.<br/>"
				+ "Chaque joueur dispose au départ de 20 Twist-Lock.<br/>" 
				+ "A tour de rôle chaque joueur désignera :<br/>"
				+ "<ul><li> un conteneur, identifié par :"
				+ "<ul><li> une ligne (1 à 10, si le tablier compte 10 lignes),</li>" 
				+ "<li>une colonne (A à G, le tablier compte 7 colonnes)</li></ul></li>" 
				+ "<li> et le coin, sur lequel il souhaite poser son twist-lock.</li></ul>" 
				+ "Chaque coin étant identifié par un numéro de 1 à 4." 
				+ "Chaque conteneur possède également une valeur aléatoire comprise entre 5 et 54.<br/><br/>"
				+ "L’objectif pour un joueur sera de prendre possession des conteneurs rapportant le plus de points.<br/>" 
				+ "Lorsqu’un joueur pose un twist lock sur un coin d’un conteneur donné, le twist-lock verrouille<br/>" 
				+ "tous les conteneurs autour de ce point.<br/>" 
				+ "Un joueur possède un conteneur lorsqu’il y a une majorité de twist-lock de sa couleur autour du conteneur.<br/>"
				+ "<br/><br/><br/><br/>"
				+ "<html>" );
        
        gbc.gridx = 1;
        gbc.gridy = 0;

        this.add( texte, gbc );
		
        this.retour = new JButton( "Retour" );
        
        gbc.gridx = 2;
        gbc.gridy = 1;
		this.retour.addActionListener( this );
		this.add( this.retour, gbc );
	}
	
public void actionPerformed(ActionEvent e) {
		
		if ( e.getSource() == this.retour ) {
			this.frame.redimensionnerFrame( 500, 600 );
			this.frame.setResizable( false );
			this.frame.setContentPane( new PanelMenu( this.frame ) );
		}

    }
}

