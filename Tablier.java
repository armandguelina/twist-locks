


/**
 * Cette classe représente un tablier un ensemble de conteneur et un ensmeble de joueur
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 * 
 */

public class Tablier
{
	private Controleur ctrl;
	private Conteneur[][] tabConteneur;
	private Joueur[] tabJoueur;
	private Joueur joueurCourant;
	private int cptJoueur;

	public Tablier( Controleur ctrl )
	{
		this.ctrl = ctrl;
	}

	public void lancerPartie( String[] ensNomJoueur , int nbLig ,int nbCol , int nbTwist )
	{
		this.tabConteneur = new Conteneur[nbLig][nbCol];

		for( int cptLig = 0 ; cptLig < tabConteneur.length ; cptLig++ )
			for( int cptCol = 0 ; cptCol < tabConteneur[cptLig].length ; cptCol++ )
				this.tabConteneur[cptLig][cptCol] = new Conteneur( this );
		
		this.tabJoueur = new Joueur[ensNomJoueur.length];
		for( int cpt = 0 ; cpt < tabJoueur.length ; cpt++ )
			this.tabJoueur[cpt] = new Joueur( ensNomJoueur[cpt] , nbTwist );
		
		this.jouerLaPartie();
	}

	public void jouerLaPartie()
	{
		cptJoueur = 0;
		joueurCourant = this.tabJoueur[cptJoueur];

		if( this.partisEstFini() )
			this.ctrl.finDePartie("Fini ! Impossible de joueur avec 0 TwistLock");
	}

	public boolean partisEstFini()
	{
		for( int cpt = 0 ; cpt < tabJoueur.length ; cpt++ )
			if( tabJoueur[cpt].getNbTwistRestant() > 0 )
				return false;
		return true;
	}
	
	public Joueur[] getTabJoueur()
	{
		Joueur[] tmp = new Joueur[ this.tabJoueur.length ];
		for( int i = 0 ; i < tabJoueur.length ; i++ )
			tmp[i] = new Joueur( this.tabJoueur[i] );
		
		return tmp;
	}
	
	public Conteneur[][] getTabConteneur()
	{
		Conteneur[][] tmp = new Conteneur[ this.tabConteneur.length ][ this.tabConteneur[0].length ];
		for( int cptLig = 0 ; cptLig < tabConteneur.length ; cptLig++ )
			for( int cptCol = 0 ; cptCol < tabConteneur[cptLig].length ; cptCol++ )
			{
				tmp[cptLig][cptCol] = new Conteneur( this.tabConteneur[cptLig][cptCol] );
			}
				
		
		return tmp;
	}

	public Joueur getJoueurCourant()
	{
		return this.joueurCourant;
	}
	
	public void placerTwistLock( String text )
	{
		if( text.equals( "fin" ) )
		{
			// Contabilise le score a chaque pose de twist lock
			// on met tout les score a 0
			for( int cpt = 0 ; cpt < this.tabJoueur.length ; cpt++ )
				this.tabJoueur[cpt].setScore( 0 );
			
			// on parcours tout les conteneur pour recuperer le joueur a qui ils appartienent
			// pour lui ajouter la valeur du contenur ( sont numero )
			for( int cptLig = 0 ; cptLig < tabConteneur.length ; cptLig++ )
				for( int cptCol = 0 ; cptCol < tabConteneur[cptLig].length ; cptCol++ )
				{
					Conteneur tmpCtn = this.tabConteneur[cptLig][cptCol];
					if( tmpCtn.getAppartenance() != 0 )
					{
						Joueur tmpJr =this.tabJoueur[tmpCtn.getAppartenance()-1]; 
						tmpJr.setScore( tmpJr.getScore() + tmpCtn.getNumConteneur() );
					}
				}
			
			// Calcule du meilleur score
			int meilleurScore = -1;
			String strFin = "";
			for( Joueur j : this.tabJoueur )
			{
				
				if( meilleurScore < j.getScore() ) 
				{
					meilleurScore = j.getScore();
					strFin = j.getNom();
				}
				else if ( meilleurScore == j.getScore() && !strFin.equals(j.getNom()) )
					strFin += " et " + j.getNom();
			}
			
			this.ctrl.finDePartie(strFin + "avec : " + meilleurScore + " points");
		}
		
		String[] tabSaisie = text.split( ":" );
		// Permet deffectuer les action et si il y a une erreur de saisie de la par du joueur c'est une erreur
		// donc le catch enleve 2 Twist lock
		try
		{
			int numLig = Integer.parseInt( tabSaisie[0] )-1;
			int numCol = (int)(tabSaisie[1].charAt( 0 )-'A');
			int numCoin = Integer.parseInt( tabSaisie[2] )-1;
			System.out.println( " LiG : " + numLig + " numCol:" + numCol + " numCoin : " + numCoin );

			if( !this.tabConteneur[numLig][numCol].coinEstLibre( numCoin ) )
				Integer.parseInt( "Go le catch" );
				
			if( numCoin == 0 )
			{
				// chaque set est dans un try catch parce que si on pose dans un coter on peut pas
				// set en dehors
				try{this.tabConteneur[numLig]  [numCol]  .setTwistLock( 0 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig]  [numCol-1].setTwistLock( 1 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig-1][numCol-1].setTwistLock( 2 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig-1][numCol]  .setTwistLock( 3 , cptJoueur+1 );}catch(Exception e) {}
			}
			else if( numCoin == 1 )
			{
				try{this.tabConteneur[numLig]  [numCol]  .setTwistLock( 1 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig]  [numCol+1].setTwistLock( 0 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig-1][numCol]  .setTwistLock( 2 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig-1][numCol+1].setTwistLock( 3 , cptJoueur+1 );}catch(Exception e) {}
			}
			else if( numCoin == 2 )
			{
				try{this.tabConteneur[numLig]  [numCol]  .setTwistLock( 2 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig]  [numCol+1].setTwistLock( 3 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig+1][numCol+1].setTwistLock( 0 , cptJoueur+1 );}catch(Exception e) {}
				try{this.tabConteneur[numLig+1][numCol]  .setTwistLock( 1 , cptJoueur+1 );}catch(Exception e) {}
			}
			else if( numCoin == 3 )
			{
				try{this.tabConteneur[numLig]  [numCol]  .setTwistLock( 3 , cptJoueur+1 );}catch(Exception e) {}
                try{this.tabConteneur[numLig]  [numCol-1].setTwistLock( 2 , cptJoueur+1 );}catch(Exception e) {}
                try{this.tabConteneur[numLig+1][numCol-1].setTwistLock( 1 , cptJoueur+1 );}catch(Exception e) {}
                try{this.tabConteneur[numLig+1][numCol]  .setTwistLock( 0 , cptJoueur+1 );}catch(Exception e) {}
			}
			joueurCourant.decrementerNbTwistLowk( 1 );
			System.out.println( "Twist-Lock poser -1 Twist" );
		}
		catch( Exception e )
		{
			System.out.println( "Erreur, une faute a été comise dans la saisie -2 Twist." );
			joueurCourant.decrementerNbTwistLowk( 2 );
		}
		
		// Contabilise le score a chaque pose de twist lock
		// on met tout les score a 0
		for( int cpt = 0 ; cpt < this.tabJoueur.length ; cpt++ )
			this.tabJoueur[cpt].setScore( 0 );
		
		// on parcours tout les conteneur pour recuperer le joueur a qui ils appartienent
		// pour lui ajouter la valeur du contenur ( sont numero )
		for( int cptLig = 0 ; cptLig < tabConteneur.length ; cptLig++ )
			for( int cptCol = 0 ; cptCol < tabConteneur[cptLig].length ; cptCol++ )
			{
				Conteneur tmpCtn = this.tabConteneur[cptLig][cptCol];
				if( tmpCtn.getAppartenance() != 0 )
				{
					Joueur tmpJr =this.tabJoueur[tmpCtn.getAppartenance()-1]; 
					tmpJr.setScore( tmpJr.getScore() + tmpCtn.getNumConteneur() );
				}
			}
		
		// Calcule du meilleur score
		int meilleurScore = -1;
		String strFin = "";
		for( Joueur j : this.tabJoueur )
		{
			
			if( meilleurScore < j.getScore() ) 
			{
				meilleurScore = j.getScore();
				strFin = j.getNom();
			}
			else if ( meilleurScore == j.getScore() && !strFin.equals(j.getNom()) )
				strFin += " et " + j.getNom();
		}

			
		// Si la partie n'est pas fini
		if( this.partisEstFini() == false )
		{
			// on prends le joueur suivant Si il a 0 twist lock on répete
			do
			{
				cptJoueur = (cptJoueur+1) == this.tabJoueur.length ? 0 : cptJoueur+1;
				joueurCourant = this.tabJoueur[cptJoueur];
			}while( joueurCourant.getNbTwistRestant() <= 0 );
		}
		else
			this.ctrl.finDePartie(strFin + "avec : " + meilleurScore + " points");
		this.ctrl.maj();
	}
	
	public int getNbLigne()
    {
        return this.tabConteneur.length;
    }

    public int getNbColonne()
    {
        return this.tabConteneur[0].length;
    }
    
    public int getNbJoueur()
    {
    	return this.tabJoueur.length;
    }
}
