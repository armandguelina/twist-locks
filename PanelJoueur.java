

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;




/**
 * Cette classe permet d'afficher le panel de chaque joueur avec le nombre de twistlock restant,
 * son score, son nom et d'afficher sa couleur.
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 *
 */
public class PanelJoueur extends JPanel
{
	private IHM frm;
	private Joueur joueur;
	private String couleurPhare;
	
	public PanelJoueur( IHM frm ,  Joueur joueur )
	{
		this.frm = frm;
		this.joueur = joueur; // Color.RED , Color.GREEN , Color.BLUE , Color.YELLOW
		this.setBackground( joueur.getCouleur() );
		if( this.joueur.getCouleur() == Color.RED )
			couleurPhare = "Rouge";
		else if( this.joueur.getCouleur() == Color.GREEN  )
			couleurPhare = "Vert";
		else if( this.joueur.getCouleur() == Color.BLUE )
			couleurPhare = "Bleu";
		else if( this.joueur.getCouleur() == Color.YELLOW )
			couleurPhare = "Jaune";
		this.maj();
	}
	
	public void paint( Graphics g )
    {
        super.paint(g);
        this.joueur = this.frm.getJoueur( joueur.getNumJoueur()-1 );
        // desinner phare
        try 
        {
            BufferedImage imageMap = ImageIO.read(new File("Phare"+couleurPhare+".png" ));
            g.drawImage(imageMap, this.getWidth()-170, this.getHeight() - 200, 150, 200 , null);
        }
        catch (IOException e) {}
        // dessinner le string
        this.setFont( new Font( "Arial", Font.BOLD, 20 ) );
        g.drawString( "Joueur : "+joueur.getNom() , 20 , 20 );
        g.drawString( "TwistLock : "+joueur.getNbTwistRestant() , 20 , 50 );
        g.drawString( "Score : "+joueur.getScore() , 20 , 70 );
        // dessiner les boules
        int tmp = joueur.getNbTwistRestant();
        int limite = joueur.getNbTwistRestant()>20?20:joueur.getNbTwistRestant();
        for( int i = 0 ; i < limite ; i++ )
        {
            tmp--;
            g.setColor( Color.BLACK );
            g.fillOval( 50 + 35*(i%5) , 100+35*(i/5) , 20 , 20 );
        }

        if( tmp > 0 )
            g.drawString( "+"+tmp , 70+ 354 , 60+354 );
    }
	
	public void maj()
	{
		this.repaint();
		this.revalidate();
	}
}
