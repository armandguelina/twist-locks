

import java.awt.Color;

/**
 * Cette classe représente un joueur
 * 
 * @author Chleo Binard , Armand Guelina , Thomas Leray & Ladislas Morcamp
 */
public class Joueur
{
	private static Color[] tabCouleur = { Color.RED , Color.GREEN , Color.BLUE , Color.YELLOW };
	private static int nbJoueur = 1;
	
	private int numJoueur;
	private int score;
	private int nbTwistRestant;
	
	private String nom;
	private Color  couleur;

	public Joueur( String nom , int nbTwist )
	{
		this.numJoueur = Joueur.nbJoueur++ ;
		this.couleur   = Joueur.tabCouleur[this.numJoueur - 1];
		
		this.nom = nom;
		this.nbTwistRestant = nbTwist;
		this.score = 0;
	}

	public Joueur( Joueur j )
	{
		this.numJoueur = j.numJoueur;
		this.couleur = j.couleur;
		this.nom = j.nom;
		this.nbTwistRestant = j.nbTwistRestant;
		this.score = j.score;
	}

	public void decrementerNbTwistLowk( int nbTwist )
	{
		this.nbTwistRestant -= nbTwist;
	}

	public void ajouterPoint( int nbPoint )
	{
		this.score += nbPoint;
	}

	public int getNumJoueur()
	{
		return numJoueur;
	}

	public String getNom()
	{
		return nom;
	}

	public void setScore( int score )
	{
		this.score = score;
	}

	public int getScore()
	{
		return score;
	}

	public int getNbTwistRestant()
	{
		return nbTwistRestant;
	}

	public Color getCouleur()
	{
		return couleur;
	}

}
